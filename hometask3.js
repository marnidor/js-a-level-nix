// task 1
// Напишите функцию isEmpty(obj), которая возвращает true, если у объекта нет свойств,
// иначе false.
let obj1 = {};
let obj11 = {
     "age": 23
};
function isEmpty1(obj) {
     return Object.keys(obj).length === 0;
}
console.log(isEmpty1(obj1));
console.log(isEmpty1(obj11));

// task 2
// Создайте функцию multiplyNumeric(obj), которая умножает все числовые свойства
// объекта obj на 2.
// Обратите внимание, что multiplyNumeric не нужно ничего возвращать. Следует
// напрямую изменять объект.
// P.S. Используйте typeof для проверки, что значение свойства числовое.
let obj2 = {
    age1: 23,
    age2: 25,
    age3: 29
}
function multiplyNumeric(obj) {
    for (let key in obj) {
        if (typeof obj[key] === 'number') {
            obj[key] *= 2;
        }
    }
    return obj;
}
console.log(multiplyNumeric(obj2));

// task 3
// Создайте функцию readNumber, которая будет запрашивать ввод числового значения
// до тех пор, пока посетитель его не введёт.
// функция должна возвращать числовое значение.
// Также надо разрешить пользователю остановить процесс ввода, отправив пустую
// строку или нажав «Отмена». В этом случае функция должна вернуть null.
function readNumber() {
    do {
        let str = prompt('Введите число', '');
        if (str === null || str === ' ') {
             return null;
        } else {
             return str;
        }
    } while (!isFinite(str));

}
alert(readNumber());

// task 4
// Встроенный метод Math.random() возвращает случайное число от 0 (включительно) до
// 1 (но не включая 1)
// Напишите функцию random(min, max), которая генерирует случайное число с
// плавающей точкой от min до max (но не включая max).
function random(min, max) {
     return Math.random() * (max - min) + min;
}
alert(random(1, 5));
alert(random(1, 5));
alert(random(1, 5));

// task 5
// Напишите функцию randomInteger(min, max), которая генерирует случайное целое
// (integer) число от min до max (включительно).
// Любое число из интервала min..max должно появляться с одинаковой вероятностью.
function randomInteger(min, max) {
     return Math.floor(Math.random() * (max + 1 - min)) + min;
}
alert(randomInteger(1, 5));
alert(randomInteger(1, 5));
alert(randomInteger(1, 5));

// task 6
// Напишите функцию ucFirst(str), возвращающую строку str с заглавным первым
// символом.
function ucFirst(str) {
     return str[0].toUpperCase() + str.slice(1);
}
alert(ucFirst("hello"));
alert(ucFirst("вася"));

// task 7
// Напишите функцию checkSpam(str), возвращающую true, если str содержит 'viagra' или
// 'XXX', а иначе false.
function checkSpam(str) {
     let newStr = str.toLowerCase();
     if (newStr.includes('viagra') || newStr.includes('xxx')) {
          return true;
     } else {
          return false;
     }
}
alert(checkSpam('buy ViAgRA now'));
alert(checkSpam('free xxxxx'));
alert(checkSpam("innocent rabbit"));

// task 8
// Создайте функцию truncate(str, maxlength), которая проверяет длину строки str и, если
// она превосходит maxlength, заменяет конец str на "...", так, чтобы её длина стала равна
// maxlength.
// Результатом функции должна быть та же строка, если усечение не требуется, либо,
// если необходимо, усечённая строка.
function truncate(str, maxlength) {
     let length = str.length;
     if (length > maxlength) {
          return str.slice(0, maxlength - 3) + "...";
     } else {
          return str;
     }
}
alert(truncate("Вот, что мне хотелось бы сказать на эту тему:", 20));
alert(truncate("Всем привет!", 20));

// task 9
// Есть стоимость в виде строки "$120". То есть сначала идёт знак валюты, а затем –
// число.
// Создайте функцию extractCurrencyValue(str), которая будет из такой строки выделять
// числовое значение и возвращать его.
function extractCurrencyValue(str) {
     return str.slice(1, str.length + 1);
}
alert( extractCurrencyValue('$120'));
alert( extractCurrencyValue('$420'));

// task 10
// Напишите функцию sumInput(), которая:
// • Просит пользователя ввести значения, используя prompt и сохраняет их в
// массив.
// • Заканчивает запрашивать значения, когда пользователь введёт не числовое
// значение, пустую строку или нажмёт «Отмена».
// • Подсчитывает и возвращает сумму элементов массива.
// • P.S. Ноль 0 – считается числом, не останавливайте ввод значений при вводе «0».
function sumInput() {
     let numbers = []
     while (true) {
          let number = Number(prompt("Введите число:", ' '));
          if (isNaN(number) || number === "" || !isFinite(number)) {
               break;
          }
          numbers.push(number);
     }
     let sum = 0;
     for (value of numbers) {
          sum += value;
     }
     return sum;
}
alert(sumInput());

// task 11
// На входе массив чисел, например: arr = [1, -2, 3, 4, -9, 6].
// Задача: найти непрерывный подмассив в arr, сумма элементов в котором максимальна.
// Функция getMaxSubSum(arr) должна возвращать эту сумму.
function findMaxSum(array) {
     let sum = 0;
     for (let i = 0; i < array.length; i++) {
          let continuousSum = 0;
          for (let j = i; j < array.length; j++) {
               continuousSum += array[j];
               sum = Math.max(sum, continuousSum);
          }
     }
     return sum;
}
alert(findMaxSum([-1, 2, 3, -9]));
alert(findMaxSum([2, -1, 2, 3, -9]));
alert(findMaxSum([-1, 2, 3, -9, 11]));
alert(findMaxSum([-2, -1, 1, 2]));
alert(findMaxSum([100, -9, 2, -3, 5]));
alert(findMaxSum([1, 2, 3]));
alert(findMaxSum([-1, -2, -3]));

// task 12
// Следующая функция возвращает true, если параметр age больше 18.
// В ином случае она запрашивает подтверждение через confirm и возвращает его
// результат:
function checkAge(age) {
     if (age > 18) {
          return true;
     } else {
          return confirm('Родители разрешили?');
     }
}
// alert(checkAge(15));
// alert(checkAge(33));
//Будет ли эта функция работать как-то иначе, если убрать else?
function checkAge2(age) {
     if (age > 18) {
          return true;
     }
     return confirm('Родители разрешили?');
}
alert(checkAge(15));
alert(checkAge2(33));
//Есть ли хоть одно отличие в поведении этого варианта? - Нет, различий нет.
// Будет выполняться только 1 return в зависимости от условий

// task 13
// Перепишите функцию, используя оператор '?' или '||'
// Следующая функция возвращает true, если параметр age больше 18.
// В ином случае она задаёт вопрос confirm и возвращает его результат.
function checkAge3(age) {
     return (age > 18) ? true : confirm('Родители разрешили?')
}
alert(checkAge3(15));
alert(checkAge3(33));

function checkAge4(age) {
     return (age > 18) || confirm('Родители разрешили?');
}
alert(checkAge4(15));
alert(checkAge4(33));

// task 14
// Функция min(a, b)
// Напишите функцию min(a,b), которая возвращает меньшее из чисел a и b.
function min(a,b) {
     return Math.min(a, b);
}
alert(min(2, 5));
alert(min(3, -1));
alert(min(1, 1));

// task 15
// Функция pow(x,n)
// Напишите функцию pow(x,n), которая возвращает x в степени n. Иначе говоря,
// умножает x на себя n раз и возвращает результат.
function pow(x, n) {
     return Math.pow(x, n);
}
let value1 = Number(prompt("Введите натуральное число", ''))
let value2 = Number(prompt("Введите натуральное число", ''))
alert(pow(3, 2));
alert(pow(3, 3));
alert(pow(1, 100));
alert(pow(value1, value2));

// task 16
// Перепишите с использованием функции-стрелки
// Замените код Function Expression стрелочной функцией:
function ask(question, yes, no) {
     if (confirm(question)) yes()
     else no();
}
ask(
    "Вы согласны?",
    () => alert("Вы согласились."),
    () => alert("Вы отменили выполнение.")
);

// task 17
// Ваш месячный доход - 3333 попугая. Вы хотите купить пальму за 8000
// попугаев. Вычислите, за какой промежуток времени вы насобираете на
// пальму, при условии что ваши ежемесячные расходы составляют 1750
// попугаев.
function calcMonths(salary, expense, palm) {
     let delta = salary - expense;
     return Math.floor(palm / delta) + 1;
}
alert(calcMonths(3333, 1750, 8000));

// task 18
// 1. Спросить у пользователя 10 чисел
// 2. Если число
// - положительное —> ничего не делать
// - отрицательная —> получить их сумму
// 3. Вывести сумму отрицательных чисел, которые ввел пользователь
// ВАЖНО: по условиям задачи в вашем коде должен быть только 1 prompt и
// только 1 цикл for
function culcSum() {
     let sum = 0;
     for (let i = 0; i < 10; i++) {
          let number = Number(prompt("Введите число", ''));
          if (number > 0) {
               sum += 0;
          } else {
               sum += number;
          }
     }
     return sum;
}
alert(culcSum());

// task 19
// Скопирован ли массив?
// Что выведет следующий код?
let fruits = ["Яблоки", "Груша", "Апельсин"];
// добавляем новое значение в "копию"
let shoppingCart = fruits;
shoppingCart.push("Банан");
// // что в fruits? - ["Яблоки", "Груша", "Апельсин", "Банан"]
alert( fruits.length ); // 4

// task 20
// Давайте произведём 5 операций с массивом.
// Создайте массив styles с элементами «Джаз» и «Блюз».
// Добавьте «Рок-н-ролл» в конец.
//
// Замените значение в середине на «Классика». Ваш код для поиска значения в
// середине должен работать для массивов с любой длиной.
// Удалите первый элемент массива и покажите его.
// Вставьте «Рэп» и «Регги» в начало массива.
// Массив по ходу выполнения операций:
let styles = ["Джаз", "Блюз"];
alert(styles.push("Рок-н-ролл"));
alert(styles[Math.floor((styles.length) / 2) + 1] = "Классика");
alert(styles.shift());
alert(styles.unshift("Рэп", "Регги"));

// task 21
// Каков результат? Почему?
let arr = ["a", "b"];
arr.push(function() {
     alert(this);
})
arr[2](); // выводит массив