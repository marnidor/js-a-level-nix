// task 3
// Скопировать и отсортировать массив
// У нас есть массив строк arr. Нужно получить отсортированную копию, но оставить arr
// неизменённым.
// Создайте функцию copySorted(arr), которая будет возвращать такую копию.

let arr = ["HTML", "JavaScript", "CSS"];
function copySorted(arr) {
    let copyArray = [...arr]
    return copyArray.sort();
}
let sorted = copySorted(arr);
alert(sorted); // CSS, HTML, JavaScript
alert(arr); // HTML, JavaScript, CSS (без изменений)

// task 4
// Факториал натурального числа – это число, умноженное на "себя минус один", затем на
// "себя минус два", и так далее до 1. Факториал n обозначается как n!
// Определение факториала можно записать как:
//
// n! = n * (n - 1) * (n - 2) * ...*1

function culcFactorial(n) {
    let factorial = 1;
    for (let i = 1; i <= n; i++) {
        factorial *= i;
    }
    return factorial;
}
alert(culcFactorial(5));

// task 5
// Последовательность чисел Фибоначчи определяется формулой Fn = Fn-1 + Fn-2. То
// есть, следующее число получается как сумма двух предыдущих.

function fib(n) {
    let start = 0;
    let next = 1;
    for (let i = 0; i < n; i++) {
        let temp = next;
        next += start;
        start = temp;
    }
    return start;
}
alert(fib(3)); // 2
alert(fib(7)); // 13
alert(fib(77)); // 5527939700884757