// task 2
// Какой обработчик запустится?
// В переменной button находится кнопка. Изначально на ней нет обработчиков.
// Который из обработчиков запустится? Что будет выведено при клике после выполнения
// кода?

let adv = document.getElementById("click");
adv.addEventListener("click", () => alert("1")); // 1 - не бл удалён, поэтому сработает
adv.removeEventListener("click", () => alert("1"));  // 2 - зде
adv.onclick = () => alert(2);
