// task 3
// Поиск элементов
// Вот документ с таблицей и формой. Как найти?...
// • Таблицу с id="age-table".
// • Все элементы label внутри этой таблицы (их три).
// • Первый td в этой таблице (со словом «Age»).
// • Форму form с именем name="search".
// • Первый input в этой форме.
// • Последний input в этой форме.
// Используйте код файла table.html и браузерные инструменты разработчика:
let table = document.getElementById("age-table");
console.log(table);

let label = document.getElementById("age-table").getElementsByTagName("label");
console.log(label);

let td = document.getElementById("age-table").getElementsByTagName("td")[0];
console.log(td);

let form = document.getElementsByName("search")[0];
console.log(form);

let input1 = document.getElementsByName("search")[0].getElementsByTagName("input")[0];
console.log(input1);

let input2 = document.getElementsByName("search")[0].getElementsByTagName("input")[1];
console.log(input2);

