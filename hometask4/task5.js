// task 5
// Создайте список
// Напишите интерфейс для создания списка.
// Для каждого пункта:
// 1. Запрашивайте содержимое пункта у пользователя с помощью prompt.
// 2. Создавайте элемент <li> и добавляйте его к <ul>.
// 3. Процесс прерывается, когда пользователь нажимает Esc или вводит пустую
// строку.
// Все элементы должны создаваться динамически.
// Если пользователь вводит HTML-теги -– пусть в списке они показываются как обычный
// текст.
let ul = document.createElement("ul");
document.body.append(ul);
do {
    let value = prompt("Input something", "");
    if (value === null || value === '') {
        break;
    }
    let li = document.createElement("li");
    li.textContent = value;
    ul.append(li);
} while (true);