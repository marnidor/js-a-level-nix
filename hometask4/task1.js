// task 1
// Дочерние элементы в DOM
// Как получить:
// • Напишите код, который получит элемент <div>?
// • Напишите код, который получит <ul>?
// • Напишите код, который получит второй <li> (с именем Пит)?
console.log(document.body.children[0]);
console.log(document.body.children[1]);
console.log(document.body.children[1].children[1]);
