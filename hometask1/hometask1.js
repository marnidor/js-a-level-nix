// task 1
let admin;
let name;
name = "Vlada";
admin = name;
alert(admin);

// task 2
const someCode = (date) => {
    return date;
}
const BIRTHDAY = '18.04.1982'; // можно использовать заглавные буквы для обеих констант
const age2 = someCode(BIRTHDAY);

// task 3
let name2 = "Ilya";
alert( `hello ${1}` ); // hello 1
alert( `hello ${"name"}` ); // hello name
alert( `hello ${name2}` ); // hello Ilya

// task 4
console.log("" + 1 + 0); // 10
console.log(true + false); // 1
console.log(6 / "3"); // 2
console.log("2" * "3"); // 6
console.log(4 + 5 + "px"); // 9px
console.log("$" + 4 + 5); // $9
console.log("4" - 2); // 2
console.log("4px" - 2); // NaN
console.log(7 / 0); // Infinity
console.log(" -9 " + 5); // -9 5
console.log(" -9 " - 5); // -14
console.log(null + 1); // 1
console.log(undefined + 1); // NaN

// task 5
let a = 1, b = 1;
let c = ++a; // 2
let d = b++; // 1
console.log(c);
console.log(d);

// task 6
let a1 = 2;
let x = 1 + (a1 *= 2);
console.log(a1); // 4
console.log(x); // 5

// task 7
console.log(5 > 4); // true
console.log("ананас" > "яблоко"); // false
console.log("2" > "12"); // true
console.log(undefined == null); // true
console.log(undefined === null); // false
console.log(null == "\n0\n"); // false
console.log(null === +"\n0\n"); // false

// task 8
function showName() {
    const name = document.getElementById('name').value;
    document.getElementById('yourName').innerHTML = "Your name is " + name;
}

// task 9
if ("0") {
    alert( 'Привет' ); // выведется alert
}

// task 10
function showAnswer() {
    const answer = document.getElementById('question').value;
    let response = "Правильно";
    if (answer !== "ECMAScript") {
        response = "Не знаете? ECMAScript";
    }
    document.getElementById('yourAnswer').innerHTML = response;
}

// task 11
let number = prompt('Input the number', '');
if (number > 0) {
    alert(1);
} else if(number === 0) {
    alert(0);
} else {
    alert(-1);
}

// task 12
alert( null || 2 || undefined ); // 2

// task 13
alert( alert(1) || 2 || alert(3) ); // 1 -> 2

// task 14
alert( 1 && null && 2 ); // null

// task 15
alert( alert(1) && alert(2) ); // 1 -> undefined

// task 16
alert( null || 2 && 3 || 4 ); // 3

// task 17, 18
let age = prompt('Input the number', '');
if (age >= 14 && age <= 90) {
    alert("Число находится в диапазоне от 14 до 90 ");
} else {
    alert("Число не находится в диапазоне от 14 до 90 ");
}

// task 18
let age18 = prompt('Input the number', '');
if (!(age18 >= 14 && age18 <= 90)) {
    alert("Число не находится в диапазоне от 14 до 90 ");
} else {
    alert("Число находится в диапазоне от 14 до 90 ");
}

// task 19
//выполнится первый и третий if
if (-1 || 0) {
    alert( 'first' );
}
if (-1 && 0) {
    alert( 'second' );
}
if (null || -1 && 1) {
    alert( 'third' );
}
console.log(-1 || 0); // -1
console.log(-1 && 0); // 0
console.log(null || -1 && 1); // 1

// task 20
let login = prompt("Введите логин", '');
if (login === "Админ") {
    let password = prompt("Введите пароль", '')
    if (password === "Я главный") {
        alert("Здравствуйте");
    } else if (!password) {
        alert("Отменено");
    } else {
        alert("Неверный пароль");
    }
} else if (!login) {
    alert("Отменено");
} else {
    alert("Я Вас не знаю");
}

// task 21
let a2 = prompt('a?', '');
switch (a2) {
    case '0':
        alert(0);
        break;
    case '1':
        alert(1);
        break;
    case '2': case '3':
        alert('2,3');
        break;
    default:
        alert('');
}

// task 22
let number1 = prompt("Введите целое число", '');
let num1 = Number(number1);
if (num1 > 0) {
    alert(num1 + 1);
} else {
    alert(num1);
}

// task 23
let number2 = prompt("Введите целое число", '');
let num2 = Number(number2);
if (num2 > 0) {
    alert(num2 + 1);
} else {
    alert(num2 - 2);
}

// task 24
let number3 = prompt("Введите целое число", '');
let num3 = Number(number3);
if (num3 > 0) {
    alert(num3 + 1);
} else if (num3 < 0) {
    alert(number3 - 2);
} else {
    alert(10);
}

// task 25
let count = 0;
for (let i = 0; i < 3; i++) {
    let val = [];
    val[i] = prompt("Введите целое число", '');
    if (val[i] > 0) {
        count++;
    }
}
alert(count);

// task 26
let count1 = 0;
let count2 = 0;
for (let i = 0; i < 3; i++) {
    let val = [];
    val[i] = prompt("Введите целое число", '');
    if (val[i] > 0) {
        count1++;
    } else {
        count2++;
    }
}
alert("Положительных: " + count1 + " Отрицательных: " + count2);

// task 27
let val27 = prompt("Введите целое число", '');
let val127 = prompt("Введите целое число", '');
if (val27 > val127) {
    alert(val27);
} else {
    alert(val127);
}

// task 28
let val28 = prompt("Введите целое число", '');
let val128 = prompt("Введите целое число", '');
if (val28 > val128) {
    alert(val28);
    alert(val128);
} else {
    alert(val128);
    alert(val28);
}

// task 29
let A = prompt("Введите число", '');
let B = prompt("Введите число", '');
if (A < B) {
    alert(A);
    alert(B);
} else {
    temp = A;
    A = B;
    B = temp;
    alert(A);
    alert(B);
}

// task 30
let A1 = prompt("Введите число", '');
let B1 = prompt("Введите число", '');
if (A1 === B1) {
    A1 = 0;
    B1 = 0;
    alert(A1);
    alert(B1);
} else {
    let A2 = Number(A1);
    let B2 = Number(B1);
    let temp = A2 + B2;
    A1 = temp;
    B1 = temp;
    alert(A1);
    alert(B1);
}

// task 31
let A31 = prompt("Введите число", '');
let B31 = prompt("Введите число", '');
let temp1 = 0;
let temp2 = 0;
if (A31 === B31) {
    A31 = 0;
    B31 = 0;
    alert(A31);
    alert(B31);
} else {
    if (A31 > B31) {
        temp1 = A31;
        B31 = temp1;
        alert(A31);
        alert(B31);
    } else {
        temp2 = B31;
        A31 = temp2;
        alert(A31);
        alert(B31);
    }
}

// task 32
let A32 = prompt("Введите число", '');
let B32 = prompt("Введите число", '');
let C32 = prompt("Введите число", '');
if (A32 < B32 && A32 < C32) {
    alert(A32);
} else if (B32 < A32 && B32 < C32) {
    alert(B32);
} else {
    alert(C32);
}

// task 33
let A33 = prompt("Введите число", '');
let B33 = prompt("Введите число", '');
let C33 = prompt("Введите число", '');
if ((A33 > B33 && A33 < C33) || (A33 > C33 && A33 < B33)) {
    alert(A33);
} else if ((B33 > A33 && B33 < C33) || (B33 > C33 && B33 < B33)) {
    alert(B33);
} else {
    alert(C33);
}

// task 34
let A34 = prompt("Введите число", '');
let B34 = prompt("Введите число", '');
let C34 = prompt("Введите число", '');
if (A34 < B34 && A34 < C34) {
    alert(A34);
    if (B34 > C34) {
        alert(B34);
    } else {
        alert(C34);
    }
} else if (B34 < A34 && B34 < C34) {
    alert(B34);
    if (A34 > C34) {
        alert(A34)
    } else {
        alert(C34);
    }
} else {
    alert(C34);
    if (A34 > B34) {
        alert(A34);
    } else {
        alert(B34);
    }
}

// task 35
let num135 = prompt("Введите число", '');
let num235 = prompt("Введите число", '');
let num335 = prompt("Введите число", '');
let A35 = Number(num135);
let B35 = Number(num235);
let C35 = Number(num335);
let max1;
let max2;
let sum;
if (A35 > B35 && A35 > C35) {
    max1 = A35;
    if (B35 > C35) {
        max2 = B35;
    } else {
        max2 = C35;
    }
    sum = max1 + max2;
    alert(sum);
} else if (B35 > A35 && B35 > C35) {
    max1 = B35;
    if (A35 > C35) {
        max2 = A35;
    } else {
        max2 = C35;
    }
    sum = max1 + max2;
    alert(sum);
} else {
    max1 = C35;
    if (A35 > B35) {
        max2 = A35;
    } else {
        max2 = B35;
    }
    sum = max1 + max2;
    alert(sum);
}

// task 36
let arr = [];
for (let  i = 0; i < 3; i++) {
    arr[i] = prompt("Введите число", '');
}
if ((arr[0] === arr[1]) || (arr[0] !== arr[2])) {
    alert("Порядковый номер: " + 3);
} else if ((arr[0] === arr[2]) || (arr[0] !== arr[1])) {
    alert("Порядковый номер: " + 2);
} else {
    alert("Порядковый номер: " + 1);
}
